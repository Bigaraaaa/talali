import pandas as pd
import xlsxwriter
import tkinter as tk
from tkinter import filedialog

root= tk.Tk()

canvas1 = tk.Canvas(root, width = 300, height = 300, bg = 'lightsteelblue')
canvas1.pack()

def getExcel ():
    global imported_file
    
    import_file_path = filedialog.askopenfilename()
    global file_name
    file_name = '(new)' + import_file_path.split('/')[-1]
    print(file_name)
    imported_file = pd.read_excel (import_file_path)
    print (imported_file)
    
browseButton_Excel = tk.Button(text='Import Excel File', command=getExcel, bg='green', fg='white', font=('helvetica', 12, 'bold'))
canvas1.create_window(150, 150, window=browseButton_Excel)

root.mainloop()

names_excell = []
emails = []
names = []
surnames = []
phones = []

# file = pd.read_excel('./29.11.xlsx')
df = pd.DataFrame(imported_file)

for row in df[df.columns[1]]:
    # print(row.strip())
    names_excell.append(row.strip())

for row in df[df.columns[0]]:
    emails.append(row.strip())
    
for row in df[df.columns[2]]:
    phones.append(row)


for name in names_excell:
    names.append(name.split(' ', 1)[0])
    surnames.append(name.split(' ', 1)[1])

# for i in range(len(names)):
#     print(names[i], ', ', surnames[i])

workbook = xlsxwriter.Workbook(file_name)
worksheet = workbook.add_worksheet()
bold = workbook.add_format({'bold': True})
worksheet.set_column('A:A', 35)
worksheet.set_column('B:B', 20)
worksheet.set_column('C:C', 30)
worksheet.set_column('D:D', 20)

for i in range(len(names)):
    if i == 1:
        worksheet.write('A1', 'E-MAIL', bold)
        worksheet.write('B1', 'NAME', bold)
        worksheet.write('C1', 'SURNAME', bold)
        worksheet.write('D1', 'PHONE', bold)
    else:
        worksheet.write('A' + str(i), emails[i])
        worksheet.write('B' + str(i), names[i])
        worksheet.write('C' + str(i), surnames[i])
        worksheet.write('D' + str(i), phones[i])

# worksheet.center_vertically()
workbook.close()
